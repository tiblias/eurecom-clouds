import { useState, useEffect } from 'react';
import { ref, onValue } from 'firebase/database';
import {db} from './services/firebase.js';


import './App.css';

import firebase from './services/firebase.js';
import NavBar from './components/NavBar';
import LoginScreen from './components/LoginScreen';
import DashBoard from './components/DashBoard';

var movie_tmp = [];
var favorite_tmp = [];

function App() {
  const [user, setUser] = useState(null);
  const [section, setSection] = useState("home");
  const [movies, setMovies] = useState(movie_tmp);
  const [favorites, setFavorites] = useState(favorite_tmp);
  
  const movieRef = ref(db, 'movies-list');

  useEffect(() => {
    firebase.auth().onAuthStateChanged(user => {
      setUser(user);
      var UID = firebase.auth().currentUser.uid;
      var u_ref = ref(db,'user/'+UID);

      onValue(u_ref, (snapshot) => {
        console.log("fetched favorites",snapshot.val());
        setFavorites(snapshot.val());
      });
    })
  }, [])

  useEffect(() => {
    onValue(movieRef, (snapshot) => {
      movie_tmp = [];
      const data = snapshot.val();
      data.forEach(element => {
        movie_tmp.push(element)
      });
      console.log(movie_tmp.length);
      setMovies(movie_tmp)
    });
  }, []);

  /* Displays the NavBar amd the webapp main body (DashBoard or LoginScreen) */
  return (
    <div className="App">
      <NavBar user={user} section={section} setSection={setSection} />
      <div class="columns">
        <div class="column is-one-fifth"></div>
        <div class="column">
          {user ? <DashBoard section={section} movies={movies} favorites={favorites} /> : <LoginScreen />}
        </div>
        <div class="column is-one-fifth"></div>

      </div>
    </div>
  );
}



export default App;
