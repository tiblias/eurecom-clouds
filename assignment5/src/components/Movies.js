import React from 'react';
import { useState } from 'react';
import { ref, update,remove } from 'firebase/database';
import {db} from '../services/firebase.js';
import firebase from '../services/firebase';


function Movies(props) {
    const [n_entries, setNEntries] = useState(30);
    var favorites = props.favorites || {};
    /* Functions interfacing with the db */
    function addFavorite(movie) {
        var id = movie.id;
        var UID = firebase.auth().currentUser.uid;
        const updates = {};
        updates['user/'+UID+'/' + id] = id;
        return update(ref(db), updates);
    }
    function removeFavorite(movie) {
        var id = movie.id;
        var UID = firebase.auth().currentUser.uid;
        return remove(ref(db, 'user/'+UID+'/' + id));
    }
    /* Defines list of movies to show. Favorites have a different button icon */
    var movies = props.movies ?
        props.movies.filter((movie,index) => index < n_entries).map(movie => {
            var button = favorites[movie.id] ? 
            <td><button class="button" onClick={() => removeFavorite(movie)}><h4 class="text is-size-6 has-text-link">♥</h4></button></td> :
            <td><button class="button" onClick={() => addFavorite(movie)}><h4 class="text is-size-6">❤️</h4></button></td>

             return <React.Fragment key={movie.id}>
                <tr>
                    <td><h4 class="text is-size-5 has-text-left">{movie.title}</h4></td>
                    <td><h4 class="text is-size-5">{movie.genre}</h4></td>
                    <td><h4 class="text is-size-5">{movie.year}</h4></td>
                    {button}
                </tr>
            </React.Fragment>
        }) : <tr></tr>;
    /* Defines a button to display more entries in the table. */
    var load_more = props.movies && props.movies.length > n_entries ? <td><button class="button" onClick={() => setNEntries(n_entries + 30)}>Show more entries...</button></td> : <td></td>;
    /* Defines table that will contain movies and appends them in the body. */
    return (
        <div>
            <table class="table is-fullwidth">
                <thead>
                    <tr>
                        <th><h4 class="text is-size-5 has-text-left">Title</h4></th>
                        <th><h4 class="text is-size-5">Genre</h4></th>
                        <th><h4 class="text is-size-5">Year</h4></th>
                        <th></th>

                    </tr>
                </thead>
                <tbody>
                    {movies}
                    <tr>
                        {load_more}
                    </tr>
                </tbody>
            </table>

        </div>
    );

}

export default Movies;