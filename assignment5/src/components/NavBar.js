import logo from '../images/video-player.png'; // with import
import Login from './Login';
import Logout from './Logout';

function NavBar(props) {
  /* Displays website icon, username and logout button when user is logged in, along with home and favorites buttons */
  return (
    <nav class="navbar has-background-dark" role="navigation" aria-label="main navigation">
      <div class="navbar-brand">
        <div class="navbar-item" onClick={() => props.setSection("home")}>
            <a><img src={logo} width="32" height="32"></img></a>
        </div>
      </div>

      <div id="navbarBasicExample" class="navbar-menu">
        <div class="navbar-start">



        </div>

        <div class="navbar-end">
          {props.user ?
            <div class="navbar-item">
              <p class="navbar-item has-text-white mx-2">{props.user._delegate.displayName}</p>
              <Logout />
              <a class="button mx-2" onClick={() => props.setSection("favorites")}>
                <strong class="title is-5">❤️</strong>
              </a>
              <a class="button mx-2" onClick={() => props.setSection("home")}>
                <strong class="title">🏠</strong>
              </a>
            </div>
            :
            <div class="navbar-item">
              <Login />
            </div>

          }

          <div class="buttons">


          </div>
        </div>
      </div>
    </nav>
  );
}

export default NavBar;