import { signOutWithGoogle } from '../services/firebase';

import '../App.css';

const Logout = () => {
  return (
    <div>
      <button className="button is-primary mx-2" onClick={signOutWithGoogle}><i className="fab fa-google"></i>Logout</button>
    </div>
  )
}

export default Logout;