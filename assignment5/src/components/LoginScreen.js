import logo from '../images/video-player.png';
import Login from './Login';
function LoginScreen(props) {
    return (
        <div>
            {/* Displays simple website icon along with login button */}
            <figure class="image is-inline-block">
                <img class="my-5" src={logo} alt="site-logo"></img>
            </figure>

            <Login />
        </div>
    );
}

export default LoginScreen;