import Movies from './Movies';
import { useState } from 'react';


function DashBoard(props) {
    /* Displays a search bar that filters movies based on all attributes. */
    const [query, setQuery] = useState("");
    var movies = props.movies || [];
    var favorites = props.favorites || {};
    console.log("favorites", favorites);
    function filterMovieQuery(movie, query) {
        var total_text = movie.title + " " + movie.genre + " " + movie.year;
        return total_text.toLowerCase().includes(query.toLowerCase());
    }
    function filterFavorites(movie,favorites) {
        return favorites[movie.id] != null;
    }

    var pageTitle = <h2 class="is-title-2">Unknown section</h2>;
    /* Search starts on change */
    var searchBar = movies && movies.length > 0 ? <input class="input is-primary mb-4" type="text" placeholder="Search..."
        onChange={e => setQuery(e.target.value)}
    /> : <div></div>;

    /* Depending on the section selected, shows only favorites or only non-favorites */
    var component = <p>Unknown section value: {props.section} </p>;

    if (props.section === "home") {
        pageTitle = <h2 class="title mb-3">Movie catalog</h2>;
        component = movies && movies.length > 0 ? <Movies movies={movies.filter((movie) => 
                filterMovieQuery(movie,query) && !filterFavorites(movie,favorites)
            )} favorites={favorites}/> 
            : <h2 class="is-title-2 mt-6 ">Loading...</h2>;
    } 
    else if (props.section === "favorites") {
        pageTitle = <h2 class="title mb-3">Your favorite movies</h2>;
        component = movies && movies.length > 0 ? <Movies movies={movies.filter((movie) => 
                filterMovieQuery(movie,query) && filterFavorites(movie,favorites)
            )} favorites={favorites}/> 
            : <h2 class="is-title-2 mt-6 ">Loading...</h2>;
    }
    /* Puts everything together */
    return (
        <div class="block mt-6">{pageTitle}{searchBar}{component}</div>
    );
}

export default DashBoard;