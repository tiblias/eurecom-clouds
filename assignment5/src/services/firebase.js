/* Contains all things related to firebase, imported in other parts of the code */

import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import { getDatabase } from 'firebase/database';

const firebaseConfig = {
  apiKey: "AIzaSyCKw62XN5HUe7tOYkYiavg0K5hgyaz9q1Y",
  authDomain: "clouds-assignment5-3222c.firebaseapp.com",
  databaseURL: "https://clouds-assignment5-3222c-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "clouds-assignment5-3222c",
  storageBucket: "clouds-assignment5-3222c.appspot.com",
  messagingSenderId: "96196232510",
  appId: "1:96196232510:web:bbb35cb794d8641ae1586f",
  measurementId: "G-C79PVR3MKC"
};

const app = firebase.initializeApp(firebaseConfig);
const db = getDatabase(app);

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });

export const auth = firebase.auth();
export const signInWithGoogle = () => auth.signInWithPopup(provider);
export const signOutWithGoogle = () => auth.signOut();
export { db };
export default firebase;
