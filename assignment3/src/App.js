import React from "react";
import './App.css';

class App extends React.Component {

	// Constructor
	constructor(props) {
		super(props);
        //will contain the info to display
		this.state = {
			value: [],
			DataisLoaded: false,
		};
        this.refresh = this.refresh.bind(this);
	}
	// ComponentDidMount is used to
	// execute the code
    // Launched when the site loads
	componentDidMount() {
		fetch(
            "https://povnfy328l.execute-api.us-east-2.amazonaws.com/books")
                        .then((res) => res.json())
                        .then((json) => {
                            this.setState({ //Updates the state when the website responds
                                value: json.Items,
                                DataisLoaded: true,
                            });
                        })
	}

    refresh() {
        this.setState({ //Updates the state when the website responds
            DataisLoaded: false,
        });
        fetch(
            "https://povnfy328l.execute-api.us-east-2.amazonaws.com/books")
                        .then((res) => res.json())
                        .then((json) => {
                            this.setState({ //Updates the state when the website responds
                                value: json.Items,
                                DataisLoaded: true,
                            });
                        });
        console.log(this.state.value);
    }

	render() {
        const { DataisLoaded, value } = this.state;
		if (!DataisLoaded) return <div> <h1> Please wait some time.... </h1> </div> ;
        
        return (
            <div className = "App">
                <title>My book library</title>
                <h1> View data on DynamoDB </h1> 
                <table class="center">
                    <th> Id </th>
                    <th> Title </th>
                    <th> Author </th>
                    <th> Category </th>
                    
                {
                    value.map((item) => <DBEntry data={item} refreshHandler={this.refresh}/>)
                }
                </table>

                <h1> Insert new data </h1>
                <FormNome refreshHandler={this.refresh}/> 
            </div>
	    );        
    }
}

class DBEntry extends React.Component {
	// Constructor
	constructor(props) {
		super(props);
        //will contain the info to display
		this.state = {
            isUpdating : false,
            id: props.data.id,
            title: props.data.title,
            author: props.data.author,
            category: props.data.category
		};
        this.handleDelete = this.handleDelete.bind(this);
	}
    handleDelete(event) {
        event.preventDefault();
        deleteBook(this.state.id).then(a => this.props.refreshHandler());
    }

	render() {
        if(!this.state.isUpdating)
        return (
                    <tr>
                        <td> { this.state.id }</td>
                        <td> { this.state.title } </td>
                        <td>{ this.state.author }</td>
                        <td> { this.state.category } </td>
                        <td> <button onClick={() => {this.setState({isUpdating:true})}}>Update</button> </td>
                        <td> <button onClick={this.handleDelete}>Delete</button> </td>
                    </tr>
	    );   
        
        return (
            <tr>
                <td> { this.state.id }</td>
                <td><input type="text" id="title" name="title" size="10" onChange={(event) => {this.setState({title:event.target.value})}}></input></td>
                <td><input type="text" id="author" name="author" size="10" onChange={(event) => {this.setState({author:event.target.value})}}></input></td>
                <td><input type="text" id="category" name="category" size="10" onChange={(event) => {this.setState({category:event.target.value})}}></input></td>
                <td><input type="button" id="submit" value="Update" size="10" onClick={ (event)=> {
                    event.preventDefault();
                    insertNewBook(this.state).then(a => this.props.refreshHandler());
                }}></input></td>
            </tr>
        );

    }
}

function insertNewBook(data) {
    return fetch('https://povnfy328l.execute-api.us-east-2.amazonaws.com/books', {
        method: 'PUT',
        body: JSON.stringify(data),
        mode:'cors'
    }).then(response => response.json()).then(json => alert(json));
    
}

function deleteBook(id) {
    return fetch('https://povnfy328l.execute-api.us-east-2.amazonaws.com/books/'+id, {
        method: 'DELETE',
        mode:'cors'
    }).then(response => response.json()).then(json => alert(json));
}

class FormNome extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
          id: '',
          title: '',
          author: '',
          category: '',
        };
      
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleSubmit(event) {
        event.preventDefault();
        insertNewBook(this.state).then(a => this.props.refreshHandler());
    }
  
    render() {
      return (
        <form onSubmit={this.handleSubmit}>        <label>
            Id: 
            <input type="text" value={this.state.value} onChange={(event) => {this.setState({id: event.target.value})  }} />
            Title:
            <input type="text" value={this.state.value} onChange={(event) => {this.setState({title: event.target.value})  }} />
            Author:
            <input type="text" value={this.state.value} onChange={(event) => {this.setState({author: event.target.value})  }} />
            Category:
            <input type="text" value={this.state.value} onChange={(event) => {this.setState({category: event.target.value})  }} />
            </label>
          <input type="submit" value="Submit" />
        </form>
      );
    }
  }


export default App;